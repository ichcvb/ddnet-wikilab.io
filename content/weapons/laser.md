---
title: "Laser Rifle"
weight: 50
---

The Laser Rifle is a automatic weapon that fires a straight beam that is used to unfreeze tees.  
The laser beam will reflect off walls, ending either by hitting a tee or by reaching its maximum range.  
If a tee is hit, the beam will instantly unfreeze that tee.  

Using the Laser Rifle is about learning its timings in various different scenarios to help other tees and also yourself through parts.

| Quickinfo  |       |
| ---------- | ----- |
| Gameskin   | ![Laser](/images/laser.png) |
| Crosshair  | ![Laser crosshair](/images/laser-crosshair.png) |
| Fire delay | 800ms |

### 1-Tick Unfreeze
If the tee you are unfreezing is currently in a freeze tile, it will get directly frozen again.
Howevery, just like when hit with the hammer, the hit tee will be unfrozen for a brief moment, in which it can:

- jump
- move (only very little)
- fire any weapon

By having the frozen tee hold jump and fire to shoot downwards with laser while in freeze, it will:

- get unfrozen for a brief moment, jump and shoot
  - the laser will reflect off the ground, unfreezing the jumping tee
- get unfrozen once more by its own shot

Alternatively the shooting tee can fire 2 consecutive laser shots, while the tee in the freeze jumps on the first shot and then gets unfrozen by the second one.

### Unfreezing Yourself
Many parts that include the Laser Rifle require you to unfreeze yourself while passing through horizontal of vertical walls of freeze.  
The angle that you need to shoot in depends on:

- the width of the wall of freeze  
- your speed  
- the distance between your tee and the wall, that you reflect the laser beam off

### Advanced behaviour
- a laser beams are delayed a bit every time they reflect off a wall
- since the Laser Rifle is an automatic weapon it can be held down to shoot indefinitely, also directly on unfreeze
