---
title: "Jump"
weight: 20
---

Jumps in Teeworlds allow you to maneuver vertically, in order to avoid obstacles and reach platforms you would otherwise not get.
On a jump, your tee will get propelled upwards.
The velocity will wear off over time until gravity pulls you down again.

In Teeworlds you have two types of jumps:

- you will ground jump whenever you are standing on some kind of floor
- the double jump (or air jump) is used while mid-air

{{% vid jump-height %}}

You will usually have one ground jump and one double jump at your disposal.

#### Ground Jump

The ground jump is only possible whenever you touch the ground.
Since it is programmed in the way that it sets your velocity to a certain value, it will disregard any other vertical boosts you gain at the same moment.
When started the vertical velocity is set to 13.2 and at its peak it reaches a height of <span title="5#22">5.69</span> tiles.
Once you don't touch the ground anymore, this jump becomes unavailable.

#### Double Jump

The double jump is available once you leave the ground.
The vertical velocity is set to 12 when activated.
Interestingly, since it sets your vertical velocity to a certain value like the ground jump does, you can manually use it to cancel all vertical velocity.
It doesn't reach as high as the first jump and only reaches <span title="4#20">4.62</span> tiles at its peak.
Together with the ground jump you can get up to <span title="10#10">10,31</span> tiles.

The brightness of the tees feet indicate if it has already used its double jump. When double jump is not available, the feet are darker.

You get your double jump back the next time you touch the ground.
You can double jump after jumping off the ground and also after walking off a platform.

### Advanced Behaviour

- whenever you hold the jump-key, you either trigger a jump instantly or trigger a jump as soon as it is available to you. This can be used to jump as soon as you touch the ground
- in cases where you have more than 2 jumps, the amount will divide in 1 ground jump and double jumps for the rest
- when timing a jump right before getting frozen, you will both get frozen and double jump at the same time
